package us.theappacademy.todo;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallback{
    public ArrayList<ToDoList> todoList = new ArrayList<ToDoList>();
    public int currentItem;

    @Override
    protected Fragment createFragment() {
        return new ToDoListFragment();

    }

    @Override
    protected int getLayoutResId() {
        return  R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
      //  Fragment newFragment = new ItemFragment();
      //  FragmentTransaction transaction = getFragmentManager().beginTransaction();
      //  transaction.replace(R.id.fragment_container, newFragment);
      //  transaction.addToBackStack(null);
       // transaction.commit();
    }
}




