package us.theappacademy.todo;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ToDoListHolder extends RecyclerView.ViewHolder {
    public TextView titleText;

    public ToDoListHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
