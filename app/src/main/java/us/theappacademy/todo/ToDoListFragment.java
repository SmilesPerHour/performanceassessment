package us.theappacademy.todo;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ToDoListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoList i1 = new ToDoList("1", "2", "3", "4");
        ToDoList i2 = new ToDoList("2", "2", "3", "4");
        ToDoList i3 = new ToDoList("3", "2", "3", "4");
        ToDoList i4 = new ToDoList("4", "2", "3", "4");
         
        activity.todoList.add(i1);
        activity.todoList.add(i2);
        activity.todoList.add(i3);
        activity.todoList.add(i4);
        
        ToDoListAdapter adapter = new ToDoListAdapter(activityCallback, activity.todoList);
        recyclerView.setAdapter(adapter);

        return view;
    }

    }



