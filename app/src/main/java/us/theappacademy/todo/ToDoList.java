package us.theappacademy.todo;


public class ToDoList {

    public String title;
    public String dateAdd;
    public String dateDue;
    public String category;

    public ToDoList(String title, String dateAdd, String dateDue, String category) {
        this.title = title;
        this.dateAdd = dateAdd;
        this.dateDue = dateDue;
        this.category = category;
    }

}
