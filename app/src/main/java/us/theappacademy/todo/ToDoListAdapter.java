package us.theappacademy.todo;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListHolder>{

    private ArrayList <ToDoList> todolist;
    private ActivityCallback activityCallback;

    public ToDoListAdapter(ActivityCallback activityCallback, ArrayList <ToDoList> todolist){
        this.activityCallback = activityCallback;
        this.todolist = todolist;
    }


      public ArrayList<ToDoList> gettodoPosts() {
                return todolist;
           }

    @Override
    public ToDoListHolder onCreateViewHolder(ViewGroup parent, int ViewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoListHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoListHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(todolist.get(position).title);

    }

    @Override
    public int getItemCount() {
        return todolist.size();
    }
}
